#define N 500
#define M N/5
#define QBER_TH 11

ltl f{ Eve_is_active -> <>(Done==1 && Result == 1)  }

chan auth_chan = [0] of { bit }
chan fiber1 = [0] of { bit, bit };
chan fiber2 = [0] of { bit, bit };

bit Eve_is_active = 0, Start_new_conv = 0, Finish = 0;
bit Alice_sending = 0;
bit Done = 0, Result = 0;
	
proctype receiver(chan input; chan photon_info)
{
	// Receive a photon
	bit photon_basis = 0, photon_value = 0;
start:
	input ? photon_basis, photon_value;

	bit guess_basis = 0, guessed_value = 0;
	atomic {
	if
	::true->guess_basis = 0; 
	::true->guess_basis = 1;
	fi
	if
	::photon_basis==guess_basis -> guessed_value = photon_value;
	::photon_basis!=guess_basis -> 
		if
		::true -> guessed_value = 0;
		::true -> guessed_value = 1;
		fi
	fi
	photon_info ! guess_basis,guessed_value;
	}
	goto start;
}

active proctype Alice()
{
	bit raw_key[N], basis[N], valid[N];
	bit Bobs_basis = 0, Bobs_key_bit = 0;
	int i = 0;
start:
	Start_new_conv = 0;
	i = 0;

send_photon:
	if
	::true -> basis[i] = 0;
	::true -> basis[i] = 1;
	fi
	if
	::true -> raw_key[i] = 0;
	::true -> raw_key[i] = 1;
	fi
	Alice_sending = 1;
	fiber1 ! basis[i], raw_key[i]; 
	Alice_sending = 0;
	i = i + 1;	
	if
	::(i < N) -> goto send_photon;
	::(i >= N) -> skip;
	fi

	i = 0;
check_bobs_basis:
	auth_chan ? Bobs_basis;
	if
	::Bobs_basis==basis[i] -> valid[i] = 1;
	::Bobs_basis!=basis[i] -> valid[i] = 0;
	fi
	auth_chan ! valid[i];
	i = i + 1;
	if
	::(i < N) -> goto check_bobs_basis;
	::(i >= N) -> skip;
	fi

	i = 0;
	int counter = 0;
	int err_count = 0;
check_bobs_key:
	auth_chan ? Bobs_key_bit;
	do
	::valid[i] == 1 -> break;
	::valid[i] == 0 -> i = i + 1;
	od
	if
	::raw_key[i] == Bobs_key_bit -> skip; 
	::raw_key[i] != Bobs_key_bit -> err_count = err_count + 1;
	fi
	i = i + 1;
	counter = counter + 1;
	if
	::(counter < M) -> goto check_bobs_key;
	::(counter >= M) -> skip;
	fi

	atomic {
	Done = 1;
	if
	::(100*err_count >= M*QBER_TH) -> Result = 0;
	::(100*err_count < M*QBER_TH) -> Result = 1;
	fi
	}
	printf("Error rate = %d \n" , err_count);
	printf("Result = %d \n", Result);
	do
	::(Start_new_conv == 1 && Finish == 0) -> goto start;
	::(Finish == 1) -> break;
	::else;
	od
}

active proctype Bob()
{
	chan bobs_rcv = [0] of { bit,bit };
	run receiver(fiber2, bobs_rcv);
	bit raw_key[N], guess_basis[N], correct_basis[N];
	bit basis = 0, value = 0;
	int i = 0;
start:
	i = 0;

read_photon:	
	bobs_rcv ? basis, value;
	raw_key[i] = value;
	guess_basis[i] = basis;
	i = i + 1;
	if
	::(i < N) -> goto read_photon;	
	::(i >= N) -> skip;
	fi

	i = 0;
send_basis:
	auth_chan ! guess_basis[i];
	auth_chan ? correct_basis[i];
	i = i + 1;
	if
	::(i < N) -> goto send_basis;
	::(i >= N) -> skip;
	fi

	i = 0;
	int counter = 0;
send_key:
	do
	::correct_basis[i] == 1 -> break;
	::correct_basis[i] == 0 -> i = i + 1;
	od
	auth_chan ! raw_key[i];
	i = i + 1;
	counter = counter + 1;
	if
	::(counter < M) -> goto send_key;
	::(counter >= M) -> skip;
	fi
	
	do
	::(Start_new_conv == 1 && Finish == 0) -> goto start;
	::(Finish == 1) -> break;
	::else;
	od
}

active proctype Eve()
{
	bit photon_basis = 0, photon_value = 0;
start:
	do
	::(Alice_sending == 1) -> break;
	::else;
	od
	if
	::(Eve_is_active == 0) ->
		fiber1 ? photon_basis,photon_value;
		fiber2 ! photon_basis,photon_value;
	::(Eve_is_active == 1) -> run receiver(fiber1, fiber2); goto finish;
	fi
	goto start;
finish:
}

init {
	Finish = 0;
	do
	::(Done == 1) -> break;
	::else;
	od
	Eve_is_active = 1; Start_new_conv = 1; Done = 0;
	do
	::Done==1 -> break;
	::else;
	od
	Finish = 1;
}

