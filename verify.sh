# Launch verification only with small key size (N < 10)!
mkdir work
cd work

spin -a  ../BB84.pml

#https://spinroot.com/spin/Man/Pan.html
gcc -DCOLLAPSE -DNOCLAIM -DMEMLIM=131072 -O2 -DXUSAFE -w -o pan pan.c

./pan -m10000  -a > ../verify_log.txt
